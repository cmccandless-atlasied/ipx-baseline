# IPX baseline manual update
Compatible with application updates only. Do not use with System Update packages.

## Usage

### On host
```Bash
$ ./build-manual-update.sh $PATH_TO_SAFE_UPDATE_FILE
$ scp build/manual-update.tgz iedadmin@$IPX:/var/tmp
```

### On target (root required)
```Bash
$ cd /var/tmp
$ tar -zxvf manual-update.tgz
$ ./do-update.sh
$ reboot -f
```
