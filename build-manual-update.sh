#!/bin/bash
set -e

UPDATE_FILE="$1"

if [ -z "$UPDATE_FILE" ]; then
    echo "usage: build-manual-update.sh UPDATE_FILE"
    exit 1
fi

UPDATE_NAME="$(basename "$UPDATE_FILE")"
VERSION="$(grep -oP '(?<=_v)0x[a-f\d]{4}' <<<"$UPDATE_FILE")"
BUILD="$(grep -oP "(?<=_v${VERSION}b)\d+(?=.tgz)" <<<"$UPDATE_FILE")"

ROOT="$(dirname "$0")"
STAGING="$ROOT/build"
if [ -e "$STAGING" ]; then
    rm -rf "$STAGING"
fi
mkdir -p "$STAGING"

cp "$ROOT/do-update.sh" "$STAGING"
sed -i s"/VERSION=0x1234/VERSION=${VERSION}/" "$STAGING/do-update.sh"
sed -i s"/BUILD=99/BUILD=${BUILD}/" "$STAGING/do-update.sh"
sed -i s"/UPDATE_FILE/${UPDATE_NAME}/" "$STAGING/do-update.sh"

cp "$UPDATE_FILE" "$STAGING"

cd "$STAGING"
tar -zcvf manual-update.tgz do-update.sh "$UPDATE_NAME"

