#!/bin/bash
set -ex
VERSION=0x1234
BUILD=99
FILE="UPDATE_FILE"

# Extract package
tar -zxvf /var/tmp/$FILE -C /var/tmp/

# Extract content
tar -zxvf /var/tmp/Content.tgz -C /var/tmp

# Run pre-update commands
bash /var/tmp/pre.txt
mount -o remount,rw /

# Install apps
for app_src in $(ls /var/tmp/*_${VERSION}); do
    cp "$app_src" /AtlasIED/
    app_name="$(basename "$app_src")"
    app_dst="/AtlasIED/${app_name}"
    app_link="${app_dst//_${VERSION}}"
    # app_link="${app_dst%._${VERSION}}"
    [ "$app_dst" != "$app_link" ]
    [ -n "$app_link" ]
    if [ -L "$app_link" ]; then
        app_old="$(readlink "$app_link")"
        if [ -f "$app_old" ]; then
            ln -sf "$app_old" "${app_link}.bak"
        fi
    else
        if [ -f "$app_link" ]; then
            mv "$app_link" "${app_link}.old"
            ln -sf "${app_link}.old" "${app_link}.bak"
        fi
    fi
    test -n "$app_link"
    ln -sf "$app_dst" "$app_link"
done

# Install additional files
for filedir in $(ls -d /var/volatile/tmp/file/*/); do
    dest="${filedir#/var/volatile/tmp/file}"
    [ -d "$dest" ]
    cp -rpf "$filedir/"* "$dest"
done

# Run post-update commands
bash /var/tmp/post.txt
